from __future__ import print_function

import random
import sys
import time

from termcolor import colored

import chess
import chess.svg


def print_board_t():
    dicci = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
    print(" ", end=' ')
    for i in range(8):
        # default value of `end` is '\n'
        print(colored(dicci[i], 'red'), end=' ')
    print("")

    mapa = str(board_t).split("\n")
    defa = 8
    for ey, punto in enumerate(mapa):
        print(colored(defa, 'red'), end=' ')
        print(punto)
        defa -= 1
    print

    file_ = open('board.svg', 'w')
    file_.write(chess.svg.board(board_t))
    file_.close()


def RedValue(depth):
    ls_l = []
    print ("RedValue")
    if (board_t.is_checkmate() or depth > 1):
        if board_t.is_checkmate():
            print ("Ganan las negras")
            return 1
        else:
            if board_t.is_game_over():
                return -1
            else:
                print ("Tabla o se ha alcanzado el nivel máximo")
                return 0

    leg = board_t.legal_moves
    print(leg)
    pm = pedir_movimiento()
    mov = chess.Move.from_uci(pm)
    min = 999
    if(mov in leg):
        board_t.push(mov)
        print_board_t()

    x = BlueValue(depth + 1)
    if x < min:
        min = x
    return min


def BlueValue(depth):
    ls_l = []
    print ("BlueValue")
    if (board_t.is_checkmate() or depth > 1):
        if board_t.is_checkmate():
            print ("Ganan las blancas")
            return 1
        else:
            if board_t.is_game_over():
                return -1
            else:
                print ("Tabla o se ha alcanzado el nivel máximo")
                return 0

    for i in board_t.legal_moves:
        mov = chess.Move.from_uci(str(i))
        ls_l.append(mov)
    max = -999
    position_Random = random.randrange(0, len(ls_l))
    board_t.push(ls_l[position_Random])
    print ("Move: %s " % ls_l[position_Random])
    print_board_t()
    x = RedValue(depth + 1)
    if x > max:
        max = x
    return max


def movi_valido():
    ls_l = []
    lis = board_t.legal_moves
    print (lis)
    for i in board_t.legal_moves:
        mov = chess.Move.from_uci(str(i))
        ls_l.append(mov)
    board_t.push(ls_l[random.randrange(0, len(ls_l))])


def start():
    global board_t, pm
    board_t = chess.Board()
    # board_t = chess.Board(
    #   "r1bqkbnr/pppp1ppp/2n5/4p2Q/2B1P3/8/PPPP1PPP/RNB1K1NR w KQkq - 0 1")
    print_board_t()

    while board_t.is_checkmate:
        leg = board_t.legal_moves
        print(leg)
        pm = pedir_movimiento()
        mov = chess.Move.from_uci(pm)
        if(mov in leg):
            board_t.push(mov)
            movi_valido()
            print_board_t()
        else:
            print("Movimiento incorrecto")


def startMarcelo():
    global board_t, pm
    # board_t = chess.Board()
    board_t = chess.Board(
        "r1bqkbnr/pppp1ppp/2n5/4p2Q/2B1P3/8/PPPP1PPP/RNB1K1NR b KQkq - 0 4")
    print_board_t()

    # MOVE
    print (BlueValue(0))
    """
    while board_t.is_checkmate:
        leg = board_t.legal_moves
        print(leg)
        pm = pedir_movimiento()
        mov = chess.Move.from_uci(pm)
        if(mov in leg):
            board_t.push(mov)
            movi_valido()
            print_board_t()
        else:
            print("Movimiento incorrecto")"""


def pedir_movimiento():
    ins = raw_input("Ingrese su movimiento: ")
    print(ins)
    return ins


def draw_board_t():
    try:
        mapa = str(board_t).split("\n")
    except:
        print("Error en la lectura del tablero")
        sys.exit(0)
    for i in xrange(len(mapa)):
        mapa[i] = mapa[i].replace(" ", "")

    try:
        for ey, punto in enumerate(mapa):
            for ex, cd in enumerate(punto):
                if(cd == "R"):
                    print("Torre")
    except:
        print("Error en la contruccion del tablero")

if __name__ == "__main__":
    # start()

    startMarcelo()

    """board = chess.Board(
        "r1bqkbnr/pppp1ppp/2n5/4p2Q/2B1P3/8/PPPP1PPP/RNB1K1NR b KQkq - 0 4")
    print(board)"""
