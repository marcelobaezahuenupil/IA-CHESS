# Ajedrez y Minimax
Librería importante:
  - Python-chess 0.15.3

Python-chess 0.15.3 es una biblioteca de Python referente al ajedrez con la generación de movimientos y validación de estos.

[![N|Solid](http://i1.wp.com/www.chessprogramming.net/wp-content/uploads/2015/05/python.png?fit=100%2C600)](https://pypi.python.org/pypi/python-chess) Librería.

Gracias a esta librería se trabajo con un tablero ya con jugadas iniciadas, se pretendió simular el jaque mate pastor:

    board_t = chess.Board(
        "r1bqkbnr/pppp1ppp/2n5/4p2Q/2B1P3/8/PPPP1PPP/RNB1K1NR b KQkq - 0 4")
Esta linea de código se mostraría esto:

![N|Solid](http://library.austintexas.gov/sites/default/files/u57/Mate%20al%20Pastor2.jpg)

Dada a esa posición inicial, comienza a mover el contrincante (piezas negras), que puede hacer cualquier movimiento aleatorio, que sea valido.

Al ya realizar un movimiento, podemos realizar un movimiento, por ejemplo si deseamos move el caballo que esta en la posición b1 a la posción c3, decimos:

    'b1c3' (sin comillas): Permitirá mover el caballo. También internamente se validará si el movimiento es permitido.

Minimax, es un algoritmo que permitirá saber quién ha ganado en el juego dada una expansión en forma de árbol de las jugadas que serán generadas por el propio algoritmo.
![N|Solid](http://mnemstudio.org/ai/game/images/minimax_move_tree1.gif)

Este algoritmo se divide en dos "funciones", RedValue (generará el movimiento de la persona), BlueValue (genera el movimiento de la máquina). 

El algoritmo realizado termina cuando las piezas blancas relizan el movimiento y mostrará en pantalla si es checkmate '1', si se pierde '-1', si es tabla '0'. En este último caso, también será valido, cuando aún el juego no termina.